import { describe, before, after, it } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

import { start, stop } from "../tools/mirror-server.js";

const Test = await SDT( Core );

describe( "Special character", () => {
	const ctx = {};
	const mirrors = [];

	before( Test.before( ctx, {
		plugin: true,
		projectFolder: "../project",
		options: {
			// debug: true
		},
	} ) );

	before( "starting hitchy and mirroring backend", () => {
		return Promise.all( [
			start( 23456 ),
			start( 23457 ),
		] )
			.then( ( [ mirror1, mirror2 ] ) => {
				mirrors[0] = mirror1;
				mirrors[1] = mirror2;
			} );
	} );

	after( "stopping hitchy and mirroring backend", () => {
		return Promise.all( [
			mirrors[0] && stop( mirrors[0] ),
			mirrors[1] && stop( mirrors[1] ),
		] );
	} );

	after( Test.after( ctx ) );


	describe( "such as whitespace", () => {
		it( "is handled on GET request forwarded w/o prefix", () => {
			return ctx.get( "/mirror/some/fake/url/with%20whitespace" )
				.then( res => {
					res.should.have.status( 200 );
					res.should.have.contentType( "application/json" );

					res.data.should.be.Object();
					res.data.method.should.be.String().which.is.equal( "GET" );
					res.data.url.should.be.String().which.is.equal( "/some/fake/url/with%20whitespace" );
				} );
		} );

		it( "is handled on GET request forwarded w/ prefix", () => {
			return ctx.get( "/mirror/prefixed/some/fake/url/with%20whitespace" )
				.then( res => {
					res.should.have.status( 200 );
					res.should.have.contentType( "application/json" );

					res.data.should.be.Object();
					res.data.method.should.be.String().which.is.equal( "GET" );
					res.data.url.should.be.String().which.is.equal( "/backend/some/fake/url/with%20whitespace" );
				} );
		} );
	} );


	describe( "such as unencoded German umlaut", () => {
		it( "is handled on GET request forwarded w/o prefix", async() => {
			const res = await ctx.get( "/mirror/some/fake/url/enthält-umlaut" );

			res.should.have.status( 200 );
			res.should.have.contentType( "application/json" );

			res.data.should.be.Object();
			res.data.method.should.be.String().which.is.equal( "GET" );
			res.data.url.should.be.String().which.is.equal( "/some/fake/url/enth%C3%A4lt-umlaut" );
		} );

		it( "is handled on GET request forwarded w/ prefix", () => {
			return ctx.get( "/mirror/prefixed/some/fake/url/enthält-umlaut" )
				.then( res => {
					res.should.have.status( 200 );
					res.should.have.contentType( "application/json" );

					res.data.should.be.Object();
					res.data.method.should.be.String().which.is.equal( "GET" );
					res.data.url.should.be.String().which.is.equal( "/backend/some/fake/url/enth%C3%A4lt-umlaut" );
				} );
		} );
	} );


	describe( "such as encoded German umlaut", () => {
		it( "is handled on GET request forwarded w/o prefix", () => {
			return ctx.get( "/mirror/some/fake/url/enth%C3%A4lt-umlaut" )
				.then( res => {
					res.should.have.status( 200 );
					res.should.have.contentType( "application/json" );

					res.data.should.be.Object();
					res.data.method.should.be.String().which.is.equal( "GET" );
					res.data.url.should.be.String().which.is.equal( "/some/fake/url/enth%C3%A4lt-umlaut" );
				} );
		} );

		it( "is handled on GET request forwarded w/ prefix", () => {
			return ctx.get( "/mirror/prefixed/some/fake/url/enth%C3%A4lt-umlaut" )
				.then( res => {
					res.should.have.status( 200 );
					res.should.have.contentType( "application/json" );

					res.data.should.be.Object();
					res.data.method.should.be.String().which.is.equal( "GET" );
					res.data.url.should.be.String().which.is.equal( "/backend/some/fake/url/enth%C3%A4lt-umlaut" );
				} );
		} );
	} );


	describe( "such as unencoded non-ASCII letter", () => {
		it( "is handled on GET request forwarded w/o prefix", async() => {
			const res = await ctx.get( "/mirror/some/fake/url/仿.copy" );

			res.should.have.status( 200 );
			res.should.have.contentType( "application/json" );

			res.data.should.be.Object();
			res.data.method.should.be.String().which.is.equal( "GET" );
			res.data.url.should.be.String().which.is.equal( "/some/fake/url/%E4%BB%BF.copy" );
		} );

		it( "is handled on GET request forwarded w/ prefix", async() => {
			const res = await ctx.get( "/mirror/prefixed/some/fake/url/仿.copy" );

			res.should.have.status( 200 );
			res.should.have.contentType( "application/json" );

			res.data.should.be.Object();
			res.data.method.should.be.String().which.is.equal( "GET" );
			res.data.url.should.be.String().which.is.equal( "/backend/some/fake/url/%E4%BB%BF.copy" );
		} );
	} );


	describe( "such as encoded non-ASCII letter", () => {
		it( "is handled on GET request forwarded w/o prefix", () => {
			return ctx.get( "/mirror/some/fake/url/%E4%BB%BF.copy" )
				.then( res => {
					res.should.have.status( 200 );
					res.should.have.contentType( "application/json" );

					res.data.should.be.Object();
					res.data.method.should.be.String().which.is.equal( "GET" );
					res.data.url.should.be.String().which.is.equal( "/some/fake/url/%E4%BB%BF.copy" );
				} );
		} );

		it( "is handled on GET request forwarded w/ prefix", () => {
			return ctx.get( "/mirror/prefixed/some/fake/url/%E4%BB%BF.copy" )
				.then( res => {
					res.should.have.status( 200 );
					res.should.have.contentType( "application/json" );

					res.data.should.be.Object();
					res.data.method.should.be.String().which.is.equal( "GET" );
					res.data.url.should.be.String().which.is.equal( "/backend/some/fake/url/%E4%BB%BF.copy" );
				} );
		} );
	} );
} );
