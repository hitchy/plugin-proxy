import { describe, before, after, it } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

import { start, stop } from "../tools/mirror-server.js";

const Test = await SDT( Core );

describe( "Forwarding requests", () => {
	const ctx = {};
	let mirror;

	before( async() => { mirror = await start( 23456 ); } );
	after( async() => { await stop( mirror ); } );

	describe( "without a request header filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
}];`,
			},
		} ) );

		it( "forwards custom request header to remote service", async() => {
			const res = await ctx.get( "/mirror/some/fake/url", { "x-foo": "found" } );

			res.data.headers.should.have.ownProperty( "x-foo" );
		} );
	} );

	describe( "with a request header filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
	filters: { request: { header: ${requestHeader} } },
}];`,
			},
		} ) );

		it( "drops custom request header", async() => {
			const res = await ctx.get( "/mirror/some/fake/url", { "x-foo": "found" } );

			res.data.headers.should.not.have.ownProperty( "x-foo" );
		} );
	} );

	describe( "without a response header filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
}];`,
			},
		} ) );

		it( "does not have custom response header", async() => {
			( await ctx.get( "/mirror/some/fake/url" ) ).headers.should.not.have.property( "x-foo" );
		} );
	} );

	describe( "with a response header filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
	filters: { response: { header: ${responseHeader} } },
}];`,
			},
		} ) );

		it( "does not have custom response header", async() => {
			( await ctx.get( "/mirror/some/fake/url" ) ).headers.should.have.property( "x-foo" ).which.is.equal( "not-set" );
			( await ctx.get( "/mirror/some/fake/url", { "x-foo": "found" } ) ).headers.should.have.property( "x-foo" ).which.is.equal( "found" );
		} );
	} );

	describe( "without a request body filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
}];`,
			},
		} ) );

		it( "does not convert transmitted text to uppercase", async() => {
			const res = await ctx.post( "/mirror/some/fake/url", "hello world!" );

			res.data.body.should.be.String().which.is.equal( "hello world!" );
		} );
	} );

	describe( "with a request body filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
	filters: { request: { body: ${requestBody} } },
}];`,
			},
		} ) );

		it( "converts transmitted text to uppercase", async() => {
			const res = await ctx.post( "/mirror/some/fake/url", "hello world!" );

			res.data.body.should.be.String().which.is.equal( "HELLO WORLD!" );
		} );
	} );

	describe( "without a response body filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
}];`,
			},
		} ) );

		it( "renames property of returned JSON data", async() => {
			const res = await ctx.post( "/mirror/some/fake/url", "hello world!" );

			res.data.body.should.be.String().which.is.equal( "hello world!" );
			( res.data.BOOH == null ).should.be.true();
		} );
	} );

	describe( "with a response body filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
	filters: { response: { body: ${responseBody} } },
}];`,
			},
		} ) );

		it( "renames property of returned JSON data", async() => {
			const res = await ctx.post( "/mirror/some/fake/url", "hello world!" );

			( res.data.body == null ).should.be.true();
			res.data.BOOH.should.be.String().which.is.equal( "hello world!" );
		} );
	} );

	describe( "with a different response body filter", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			files: {
				"config/proxy.js": `
exports.proxy = [{
	prefix: "/mirror",
	target: "http://127.0.0.1:23456/",
	filters: { response: { body: ${responseBodyAlt} } },
}];`,
			},
		} ) );

		it( "renames property of returned JSON data and adjusts size of response payload", async() => {
			const res = await ctx.post( "/mirror/some/fake/url", "hello world!" );

			( res.data.body == null ).should.be.true();
			( res.data.BOOH == null ).should.be.true();
			res.data.BUDDY.should.be.String().which.is.equal( "hello world!" );
		} );
	} );
} );

// eslint-disable-next-line jsdoc/require-jsdoc
function requestHeader( req ) {
	if ( req.headers["x-foo"] ) {
		delete req.headers["x-foo"];
	}
}

// eslint-disable-next-line jsdoc/require-jsdoc
function responseHeader( res, req, forwardReq ) {
	res.headers["x-foo"] = forwardReq.getHeader( "x-foo" ) || "not-set";
}

// eslint-disable-next-line jsdoc/require-jsdoc
function requestBody() {
	return new ( require( "stream" ).Transform )( {
		transform( chunk, encoding, done ) {
			done( null, Buffer.from( chunk.toString( "utf8" ).toUpperCase(), "utf8" ) );
		}
	} );
}

// eslint-disable-next-line jsdoc/require-jsdoc
function responseBody() {
	const chunks = [];

	return new ( require( "stream" ).Transform )( {
		transform( chunk, encoding, done ) {
			chunks.push( chunk );
			done();
		},

		flush( done ) {
			done( null, Buffer.from( Buffer.concat( chunks ).toString( "utf8" ).replace( /"body"/g, '"BOOH"' ), "utf8" ) );
		}
	} );
}

// eslint-disable-next-line jsdoc/require-jsdoc
function responseBodyAlt( _, __, ___, res ) {
	const chunks = [];

	return new ( require( "stream" ).Transform )( {
		transform( chunk, encoding, done ) {
			chunks.push( chunk );
			done();
		},

		flush( done ) {
			const data = Buffer.from( Buffer.concat( chunks ).toString( "utf8" ).replace( /"body"/g, '"BUDDY"' ), "utf8" );

			res.set( "content-length", data.length );

			done( null, data );
		}
	} );
}
