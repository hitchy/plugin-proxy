import { describe, before, after, it } from "mocha";

import "should";
import "should-http";

import { start as StartHTTP, stop as StopHTTP } from "../tools/mirror-server.js";
import HttpTagString from "http-tag-string";

const HTTP = HttpTagString( "http://127.0.0.1:23456" );


describe( "Some special server for help testing proxy functionality", () => {
	let server = null;

	before( "starting special HTTP server", () => {
		return StartHTTP()
			.then( runningServer => {
				server = runningServer;
			} );
	} );

	after( "stopping special HTTP server", () => {
		return server ? StopHTTP( server ) : null;
	} );

	it( "GETs JSON object", () => {
		return HTTP`GET /some/url`()
			.then( response => {
				response.should.have.status( 200 );

				return response.json()
					.then( json => {
						json.should.be.Object();
						json.method.should.be.String().which.is.equal( "GET" );
						json.url.should.be.String().which.is.equal( "/some/url" );
						json.headers.should.be.Object();
						json.body.should.be.String().which.is.empty();
					} );
			} );
	} );

	it( "POSTs JSON object", () => {
		return HTTP`POST /some/url?arg=1&alt=two
		Content-Type: application/json`( {
			prop: "sent",
		} )
			.then( response => {
				response.should.have.status( 200 );

				return response.json()
					.then( json => {
						json.should.be.Object();
						json.method.should.be.String().which.is.equal( "POST" );
						json.url.should.be.String().which.is.equal( "/some/url?arg=1&alt=two" );
						json.headers.should.be.Object();
						json.headers.should.have.property( "content-type" ).which.is.equal( "application/json" );
						json.body.should.be.String().which.is.equal( `{"prop":"sent"}` );
					} );
			} );
	} );

	it( "PUTs JSON object", () => {
		return HTTP`PUT /some/url?arg=1&alt=two
		Content-Type: application/json
		X-Special-Info: here it comes`( {
			prop: "sent",
		} )
			.then( response => {
				response.should.have.status( 200 );

				return response.json()
					.then( json => {
						json.should.be.Object();
						json.method.should.be.String().which.is.equal( "PUT" );
						json.url.should.be.String().which.is.equal( "/some/url?arg=1&alt=two" );
						json.headers.should.be.Object();
						json.headers.should.have.property( "content-type" ).which.is.equal( "application/json" );
						json.headers.should.have.property( "x-special-info" ).which.is.equal( "here it comes" );
						json.body.should.be.String().which.is.equal( `{"prop":"sent"}` );
					} );
			} );
	} );

	it( "DELETEs JSON object", () => {
		return HTTP`DELETE /some/url?arg=1&alt=two
		Content-Type: application/json
		X-Special-Info: here it comes`()
			.then( response => {
				response.should.have.status( 200 );

				return response.json()
					.then( json => {
						json.should.be.Object();
						json.method.should.be.String().which.is.equal( "DELETE" );
						json.url.should.be.String().which.is.equal( "/some/url?arg=1&alt=two" );
						json.headers.should.be.Object();
						json.headers.should.have.property( "content-type" ).which.is.equal( "application/json" );
						json.headers.should.have.property( "x-special-info" ).which.is.equal( "here it comes" );
						json.body.should.be.String().which.is.empty();
					} );
			} );
	} );

	it( "OPTIONSes JSON object", () => {
		return HTTP`OPTIONS /some/url?arg=1&alt=two
		X-Special-Info: here it comes`()
			.then( response => {
				response.should.have.status( 200 );

				return response.json()
					.then( json => {
						json.should.be.Object();
						json.method.should.be.String().which.is.equal( "OPTIONS" );
						json.url.should.be.String().which.is.equal( "/some/url?arg=1&alt=two" );
						json.headers.should.be.Object();
						json.headers.should.have.property( "x-special-info" ).which.is.equal( "here it comes" );
						json.body.should.be.String().which.is.empty();
					} );
			} );
	} );

	it( "GETs value-less query parameters properly", () => {
		return HTTP`GET /some/url?arg`()
			.then( response => {
				response.should.have.status( 200 );

				return response.json()
					.then( json => {
						json.should.be.Object();
						json.url.should.be.String().which.is.equal( "/some/url?arg" );
					} );
			} );
	} );
} );
