import { describe, before, after, it } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

describe( "Hitchy instance with proxy", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: "../project",
	} ) );

	it( "is running", () => {
		return ctx.get( "/status" )
			.then( res => {
				res.should.have.status( 200 );
			} );
	} );

	it( "is providing data fetched from remote target", () => {
		return ctx.get( "/ddg/assets/dax.svg" )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "image/svg+xml" );
			} );
	} );
} );
