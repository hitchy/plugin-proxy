import HTTP from "node:http";

/**
 * Starts HTTP server responding to every incoming request with JSON data set
 * describing properties of either request.
 *
 * @param {int} port number of port mirror is available at locally
 * @return {Promise<HTTP.Server>} promises HTTP server listening on port 23456
 */
export function start( port = 23456 ) {
	return new Promise( ( resolve, reject ) => {
		const server = HTTP.createServer( ( req, res ) => {
			const chunks = [];

			req.on( "data", chunk => chunks.push( chunk ) );

			req.on( "end", () => {
				const data = {
					method: req.method,
					url: req.url,
					headers: req.headers,
					body: Buffer.concat( chunks ).toString( "utf8" ),
				};

				const match = /\/redirect\/me\/(30\d)\?to=([^&]+)/.exec( req.url );
				if ( match ) {
					res.statusCode = parseInt( match[1] );
					res.setHeader( "Location", decodeURIComponent( match[2] ) );
				} else {
					res.statusCode = 200;
				}

				res.setHeader( "content-type", "application/json" );
				res.end( Buffer.from( JSON.stringify( data ), "utf8" ) );
			} );
		} );

		server.on( "error", reject );
		server.on( "listening", () => resolve( server ) );

		server.listen( port, "127.0.0.1" );
	} );
}

/**
 * Stops server returned from previous call of `start()`.
 *
 * @param {HTTP.Server} server instance of server to be stopped
 * @return {Promise} promises server stopped actually
 */
export function stop( server ) {
	return new Promise( resolve => {
		server.close();

		server.on( "close", resolve );
	} );
}
