"use strict";

exports.routes = {
	"/status": function( req, res ) {
		res.status( 200 ).send();
	},
};
