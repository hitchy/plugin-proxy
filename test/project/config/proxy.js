export const proxy = [
	{
		prefix: "/ddg",
		target: "https://duckduckgo.com/",
	},
	{
		prefix: "/mirror",
		target: "http://127.0.0.1:23456/",
	},
	{
		prefix: "/mirror/prefixed",
		target: "http://127.0.0.1:23457/backend",
	},
	{
		prefix: "/alt/mirror/prefixed",
		target: "http://127.0.0.1:23457/backend",
	},
	{
		prefix: "/alt/mirror/sibling",
		target: "http://127.0.0.1:23457/separate",
	},
	{
		prefix: "/alt/mirror/sub",
		target: "http://127.0.0.1:23457/backend/sub",
	},
];
