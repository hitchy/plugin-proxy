# @hitchy/plugin-proxy [![pipeline status](https://gitlab.com/hitchy/plugin-proxy/badges/master/pipeline.svg)](https://gitlab.com/hitchy/plugin-proxy/-/commits/master)

_Hitchy plugin implementing reverse proxy for redirecting selected requests to remote servers_

## License

[MIT](LICENSE)

## Installation

```bash
npm i @hitchy/plugin-proxy
```

## Configuration

Proxies are defined as part of your application's configuration under top-level name `proxy`. In compliance with Hitchy's conventions you should create a file **config/proxy.js** in your Hitchy-based application. The proxy configuration itself is an array of objects each one defining another reverse proxy.

Every such configuration object **must consist** of these mandatory properties:

* `prefix` is declaring a local a routing prefix. All incoming requests matching this prefix are processed as part of this proxy definition.

  Due to the way Hitchy is handling its routing table, other plugins or current application may define more specific prefixes used in preference over any prefix defined here. Thus, it is possible to use prefix `/` here and still have a local API with prefix `/api` or similar.
  
* `target` provides base URL of a remote service all processed requests are forwarded to.

In addition, either configuration object **may include** any of these optional properties:

* `alias` is providing one or more aliases. [See below](#aliases) for additional information.

* `filters` _(since v0.2.0)_ is optionally providing custom callbacks for adjusting header and body of request and/or response on the fly.

  > Prior to version v0.2.0, option `responseFilter` was supported, only. It is called `filters.response.header`, now.

  * `filters.request.header` is a function invoked early on receiving request from client to be forwarded to some remote service. It is designed to inspect and adjust client's request header. 

    Provided argument is the [request originally received from client](https://nodejs.org/dist/latest/docs/api/http.html#http_class_http_incomingmessage).  

    The function may optionally adjust provided request or return a replacement for it. It may return a promise to delay this information, too.
  
    ```javascript
    filters.request.header = req => {
        if ( req.getHeader( "cookie" ) ) {
            req.removeHeader( "cookie" );
        }
    }
    ```

  * `filters.request.body` is a function invoked on receiving request from client to be forwarded to some remote service. It is expected to return a [transform stream instance](https://nodejs.org/dist/latest/docs/api/stream.html#class-streamtransform) to be injected into [pipeline](https://nodejs.org/dist/latest/docs/api/stream.html#streampipelinesource-transforms-destination-callback) forwarding the request's body to the remote service. 

    Provided argument is the [optionally filtered request received from client](https://nodejs.org/dist/latest/docs/api/http.html#http_class_http_incomingmessage).

    It _may_ return a [transform stream instance](https://nodejs.org/dist/latest/docs/api/stream.html#class-streamtransform) to be considered in forwarding the request's payload. On returning any other value, request's payload is forwarded to remote service as-is.
  
    ```javascript
    filters.request.body = req => {
        return new Transform( {
            transform( chunk, encoding, doneFn ) {
                // TODO implement your transformation per chunk here
                doneFn( null, chunk );
            }        
        } );
    }
    ```

  * `filters.response.header` is a function invoked early on receiving response from remote service. It is designed to adjust its headers prior to processing and forwarding it to the originally requesting client. 

    Provided arguments are 
  
    1. the [response received from remote service](https://nodejs.org/dist/latest/docs/api/http.html#class-httpserverresponse), 
    2. the [optionally filtered request originally received from client](https://nodejs.org/dist/latest/docs/api/http.html#http_class_http_incomingmessage) and 
    3. the [request forwarded to the remote service](https://nodejs.org/dist/latest/docs/api/http.html#http_class_http_clientrequest).  

    The function may optionally adjust response provided in first argument or return a replacement for it. It may return a promise to delay this information, too.
  
    ```javascript
    filters.response.header = res => {
        if ( res.getHeader( "set-cookie" ) ) {
            res.removeHeader( "set-cookie" );
        }
    }
    ```

  * `filters.response.body` is a function invoked on receiving response from remote service to be forwarded to originally requesting client. It is expected to return a [transform stream instance](https://nodejs.org/dist/latest/docs/api/stream.html#class-streamtransform) to be injected into [pipeline](https://nodejs.org/dist/latest/docs/api/stream.html#streampipelinesource-transforms-destination-callback) forwarding the response's body to the client.

    Provided arguments are 
  
    1. the [optionally filtered response received from remote service](https://nodejs.org/dist/latest/docs/api/http.html#class-httpserverresponse), 
    2. the [optionally filtered request originally received from client](https://nodejs.org/dist/latest/docs/api/http.html#http_class_http_incomingmessage) and 
    3. the [request forwarded to the remote service](https://nodejs.org/dist/latest/docs/api/http.html#http_class_http_clientrequest).  

    It _may_ return a [transform stream instance](https://nodejs.org/dist/latest/docs/api/stream.html#class-streamtransform) to be considered in forwarding the response's payload. On returning any other value, response's payload is forwarded to remote service as-is.

    ```javascript
    filters.response.body = req => {
        return new Transform( {
            transform( chunk, encoding, doneFn ) {
                // TODO implement your transformation per chunk here
                doneFn( null, chunk );
            }        
        } );
    }
    ```

* `opaque` is a boolean switch. When set, forwarded requests do not contain headers exposing IP addresses of actual clients any request has been forwarded for. This includes `Forwarded-For`,`X-Forwarded-For`, `X-Forwarded-Host`, `X-Forwarded-Proto` and `X-Real-IP`.


### Example

```javascript
exports.proxy = [
    {
        prefix: "/ddg",
        target: "https://duckduckgo.com/",
        alias: "https://ddg.com/",
    },
    {
        prefix: "/denic",
        target: "https://www.denic.de",
    },
    {
        prefix: "/denic-alt",
        target: "https://www.denic.de",
        filters: {
            response: {
                header( backendResponse ) {
                    if ( backendResponse.statusCode === 401 ) {
                        backendResponse.statusCode = 403;
                        backendResponse.removeHeader( "WWW-Authenticate" );
                    }
                },
            },
        },
        opaque: true,
    },
];
```

This configuration file is defining three reverse proxies: 

* The first one is forwarding all requests for routes starting with **/ddg** to the base URL **https://duckduckgo.com** so that requesting **/ddg/assets/dax.svg** will eventually deliver the resource available at **https://duckduckgo.com/assets/dax.svg**.

* The second one is forwarding requests with prefix **/denic** to **http://www.denic.de**.

* The third one is basically working like second one. However, all responses with status 401 are transformed into responses with status 403.

### Aliases

Every proxy may declare one or more aliases used on mapping URls returned from target back into address space used in request sent to the proxy itself. Aliases are useful e.g. to test and develop backends prepared to run at different URL when in production setup.

> **Example**
>
> In first example above a forwarded request might cause response redirecting to URL `https://duckduckgo.com/index.html` which is translated back to redirecting to `/ddg/index.html` prior to returning it to the client. However, this translation fails when remote service redirects to `https://ddg.com/index.html` instead for it is assumed to be an alias for the same site. By adopting this alias in your configuration even this URL gets translated back to `/ddg/index.html` properly.

Use custom filters as described above for a more sophisticated address translation.
