import { posix as Path } from "node:path";
import HTTP from "node:http";
import HTTPS from "node:https";
import { Transform, pipeline } from "node:stream";

const defaultAgentOptions = {
	keepAlive: true,
};

/**
 * Declares blueprint route for every configuration of a proxy.
 *
 * @returns {Map<string, function>} maps from routes in functions handling requests for either route
 */
export function blueprints() {
	const { config: { proxy: proxies }, log } = this;

	const routes = [];
	const reverseMap = {};
	const logDebug = log( "hitchy:proxy:debug" );
	const logInfo = log( "hitchy:proxy:info" );
	const logError = log( "hitchy:proxy:error" );


	if ( Array.isArray( proxies ) ) {
		const numProxies = proxies.length;

		logDebug( "found configuration for %d reverse proxies", numProxies );

		for ( let i = 0; i < numProxies; i++ ) {
			const proxy = proxies[i] || {};
			const { prefix, target } = proxy;

			if ( prefix && target ) {
				const parsed = new URL( target );

				if ( parsed.hostname ) {
					const normalizedPrefix = prefix.replace( /(?<!^)\/{1,16}$/, "" );
					const agentOptions = Object.assign( {}, defaultAgentOptions, proxy.agentOptions );
					const context = {
						prefix: normalizedPrefix,
					};

					if ( parsed.protocol === "https:" ) {
						context.library = HTTPS;
						context.agent = new HTTPS.Agent( agentOptions );
						context.defaultPort = 443;
					} else {
						context.library = HTTP;
						context.agent = new HTTP.Agent( agentOptions );
						context.defaultPort = 80;
					}

					if ( !parsed.port ) {
						parsed.port = context.defaultPort;
					}

					const href = parsed.toString();

					logDebug( "- forwarding %s to %s", normalizedPrefix, href );

					const proxyHandler = createProxy.call( { logDebug, logInfo, logError }, context, parsed, proxy, reverseMap );

					routes.push( [ ( normalizedPrefix === "/" ? "" : normalizedPrefix ) + "{/*route}", proxyHandler ] );

					if ( reverseMap.hasOwnProperty( href ) ) {
						reverseMap[href].push( normalizedPrefix );
					} else {
						reverseMap[href] = [normalizedPrefix];
					}
				}
			}
		}

		for ( let i = 0; i < numProxies; i++ ) {
			const { prefix, alias } = proxies[i] || {};

			if ( prefix && alias ) {
				const aliases = Array.isArray( alias ) ? alias : [alias];
				const numAliases = aliases.length;

				for ( let j = 0; j < numAliases; j++ ) {
					const _alias = aliases[j];

					if ( _alias && typeof _alias === "string" ) {
						const url = new URL( _alias );

						if ( url.hostname ) {
							const port = url.port || ( url.protocol === "https:" ? 443 : 80 );
							const search = url.search ? "?" + url.search : "";
							const normalized = `${url.protocol}//${url.hostname}:${port}${url.pathname}${search}`;

							if ( !reverseMap.hasOwnProperty( normalized ) ) { // eslint-disable-line max-depth
								reverseMap[normalized] = [prefix];
							}
						}
					}
				}
			}
		}
	}

	return new Map( routes.sort( ( l, r ) => r[0].localeCompare( l[0] ) ) );
}

const pass = i => i;
const nop = () => {}; // eslint-disable-line no-empty-function

/**
 * Generates request handler forwarding requests to some remote target.
 *
 * @param {string} prefix local URL prefix addressing proxy to be created
 * @param {URL} backend parsed base URL of proxy's remote target
 * @param {object} config configuration of proxy to be created
 * @param {Object<string,string>} reverseMap maps from a target's absolute URL back to the prefix of related reverse proxy
 * @param {object} library set of methods to use on sending HTTP requests as a client
 * @param {Agent} agent HTTP agent to use on forwarding requests to defined target
 * @returns {Hitchy.Core.RequestControllerHandler} generated request handler
 */
function createProxy( { prefix, library, agent }, backend, config, reverseMap ) {
	const { hideHeaders = [], timeout = 5000, filters = {} } = config;
	const { logDebug, logInfo, logError } = this;

	const { request: requestFilter = {} , response: responseFilter = {} } = filters;
	const { header: requestHeaderFilter = pass, body: requestBodyFilter = nop } = requestFilter;
	const { header: responseHeaderFilter = pass, body: responseBodyFilter = nop } = responseFilter;

	if ( typeof requestHeaderFilter !== "function" ||
	     typeof requestBodyFilter !== "function" ||
	     typeof responseHeaderFilter !== "function" ||
	     typeof responseBodyFilter !== "function" ) {
		throw new TypeError( "invalid filter in proxy configuration" );
	}

	return async( req, res ) => {
		const filteredReq = await requestHeaderFilter( req ) || req;
		const { route = [] } = filteredReq.params;

		// prevent client from relatively addressing URL beyond scope of current proxy's prefix
		const pathname = Path.resolve( backend.pathname, ...route );

		if ( pathname.indexOf( backend.pathname ) !== 0 && pathname + "/" !== backend.pathname ) {
			logError( "invalid path name: %s", route.join( "/" ) );

			returnError( "invalid path name", 400 );
			return;
		}


		// compile current request's query back into single string
		const queryNames = Object.keys( filteredReq.query );
		const numNames = queryNames.length;
		const query = new Array( numNames );

		for ( let i = 0; i < numNames; i++ ) {
			const name = queryNames[i];
			const value = filteredReq.query[name];
			const key = encodeURIComponent( name );

			if ( value === true ) {
				query[i] = key;
			} else {
				query[i] = key + "=" + encodeURIComponent( value );
			}
		}


		// make a (probably reduced) copy of all incoming request headers
		const client = filteredReq.socket.remoteAddress;
		const proto = filteredReq.socket.encrypted ? "https" : "http";
		const copiedHeaders = {};

		if ( !config.opaque ) {
			copiedHeaders["x-real-ip"] = client;
			copiedHeaders["x-forwarded-for"] = client;
			copiedHeaders["x-forwarded-host"] = filteredReq.headers.host;
			copiedHeaders["x-forwarded-proto"] = proto;
			copiedHeaders["forwarded"] = `for=${client.indexOf( ":" ) < 0 ? client : '"[' + client + ']"'};proto=${proto}`;
		}

		{
			const source = filteredReq.headers;
			const keys = Object.keys( source );
			const numKeys = keys.length;

			for ( let i = 0; i < numKeys; i++ ) {
				const key = keys[i];

				switch ( key ) {
					case "host" :
					case "x-real-ip" :
					case "x-forwarded-host" :
					case "x-forwarded-proto" :
						// never pass these headers
						break;

					case "x-forwarded-for" :
					case "forwarded" :
						// extend these headers
						if ( !config.opaque ) {
							copiedHeaders[key] = `${source[key]},${copiedHeaders[key]}`;
						}
						break;

					default :
						if ( hideHeaders.indexOf( key ) > -1 ) {
							// configuration rejects to pass this header
							break;
						}

						copiedHeaders[key] = source[key];
				}
			}
		}


		// create local client forwarding request to remote target
		const proxyReqOptions = {
			protocol: backend.protocol,
			host: backend.hostname,
			port: backend.port,
			method: filteredReq.method,
			path: encodeURI( pathname ) + ( query.length > 0 ? "?" + query.join( "&" ) : "" ),
			headers: copiedHeaders,
			timeout: timeout,
			agent: agent,
		};

		const proxyReqUrl = `${backend.protocol}//${backend.hostname}:${backend.port}${proxyReqOptions.path}`;

		logDebug( "forward request is %s %s with headers: %o timeout: %d",
			proxyReqOptions.method, proxyReqUrl, proxyReqOptions.headers,
			proxyReqOptions.timeout );

		const proxyReq = library.request( proxyReqOptions, async proxyRes => {
			logDebug( "response is %d %o", proxyRes.statusCode, proxyRes.headers );

			try {
				const filteredRes = await responseHeaderFilter( proxyRes, filteredReq, proxyReq ) || proxyRes;

				res.status( filteredRes.statusCode );

				// process and forward response headers sent by backend
				const source = filteredRes.headers;
				const keys = Object.keys( source );
				const numKeys = keys.length;

				for ( let i = 0; i < numKeys; i++ ) {
					const key = keys[i];

					switch ( key ) {
						case "location" : {
							const target = source[key];
							const translated = translateUrlBackendToFrontend( prefix, route, target, proxyReqUrl );

							logDebug( "translating location %s to %s", target, translated );
							res.set( "Location", translated );

							break;
						}

						default :
							res.set( key, source[key] );
					}
				}


				const backChannel = [ filteredRes, res ];
				const translator = await responseBodyFilter( filteredRes, filteredReq, proxyReq, res );

				if ( translator instanceof Transform ) {
					backChannel.splice( 1, 0, translator );
				}

				pipeline( backChannel, error => ( error ? onResponseError( error ) : undefined ) );
			} catch ( cause ) {
				onResponseError( cause );
			}
		} );


		const frontChannel = [ filteredReq, proxyReq ];
		const translator = requestBodyFilter( filteredReq );

		if ( translator instanceof Transform ) {
			frontChannel.splice( 1, 0, translator );
		}

		pipeline( frontChannel, error => ( error ? onRequestError( error ) : undefined ) );


		/**
		 * Handles error encountered while processing forward request.
		 *
		 * @param {Error} error description of encountered error
		 * @returns {void}
		 */
		function onRequestError( error ) {
			logError( "forward request failed: %s", error.stack );

			returnError( error.message );
		}

		/**
		 * Handles error encountered while processing response to forward
		 * request.
		 *
		 * @param {Error} error description of encountered error
		 * @returns {void}
		 */
		function onResponseError( error ) {
			logError( "processing response to forward request failed: %s", error.stack );

			returnError( error.message );
		}

		/**
		 * Commonly adjusts response to return provided error.
		 *
		 * @param {string} message error message to include with response
		 * @param {int} statusCode HTTP status code to use in response
		 * @returns {void}
		 */
		function returnError( message, statusCode = 502 ) {
			res
				.status( statusCode )
				.set( "x-error", message )
				.format( {
					json: () => res.json( { error: `reverse proxy error: ${message}` } ),
					xml: () => res.type( "xml" ).send( `<?xml version="1.0"?><error><![CDATA[reverse proxy error: ${message}]]></error>` ),
					default: () => res.type( "text" ).send( `reverse proxy error: ${message}` ),
				} );
		}
	};

	/**
	 * Translates URL provided by backend into URL for the proxy's client
	 * addressing the same resource via the proxy.
	 *
	 * @param {string} proxyPrefix URL prefix of current proxy
	 * @param {string[]} current segments of currently processed route in scope of proxy's prefix
	 * @param {string} target URL provided by backend to be translated back to scope of proxy's URL
	 * @param {string} backendUrl URL of request sent to eventual backend by this proxy
	 * @return {string} translated URL addressing same resource for use by frontend e.g. proxy's client
	 */
	function translateUrlBackendToFrontend( proxyPrefix, current, target, backendUrl ) {
		const qualified = new URL( target, backendUrl ).toString();
		let match = "";

		for ( const beBase of Object.keys( reverseMap ) ) {
			const base = beBase.replace( /[/?#]{1,3}$/, "" );

			if ( qualified.startsWith( base ) &&
			     base.length > match.length &&
			     /[/?#]|$/.test( qualified[base.length] ) ) {
				match = beBase;
			}
		}

		if ( match ) {
			let feBase = reverseMap[match];

			if ( Array.isArray( feBase ) ) {
				let found;

				for ( let i = 0; i < feBase.length; i++ ) {
					if ( feBase[i] === proxyPrefix ) {
						found = feBase[i];
						break;
					}
				}

				if ( found == null ) {
					feBase = feBase.sort( ( l, r ) => l.length - r.length )[0];
				} else {
					feBase = found;
				}
			}

			return feBase + qualified.substring( match.replace( /[/?#]{1,3}$/, "" ).length );
		}

		return qualified;
	}
}
